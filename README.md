# dev scripts

A set of scripts and utilities to make setting up a consistent npm project easier.

To use this package, install it as a dev dependency and then run the `setup.js` script.
